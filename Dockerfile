FROM node:16-alpine3.11

WORKDIR /main

COPY . .

RUN npm install

EXPOSE 3000

CMD ["npm", "run", "dev"]
