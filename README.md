# Typescript API

## About The Project

A simple API demo written in TypeScript with MongoDB as the data store

### Built with

- TypeScript
- MongoDB
- Docker

## Getting Started

### Prerequisites

- Docker
- Docker Compose

### Installation

This demo API works best using Docker and Docker Compose

1. Clone the repo
```
git clone https://gitlab.com/richard.ng/typescript-api.git
```

2. Set up external .env file for Docker Compose
```
API_PORT=3000
MONGO_URL=mongodb://user:password@mongo:27017/listings
```

3. Build/Run the images
```
docker-compose up --build
```

The default port is 3000

## Usage

You can test the API endpoints:

1. Get all
```
curl localhost:3000/listings
```

2. Get one
```
curl localhost:3000/listings/:title
```

3. Add new
```
curl localhost:3000/listings \
  -X POST \
  -H "Content-type:application/json" \
  -d '{"userId": 1, "id": 1, "title": "Title of listing", "body": "Description"}'
```

4. Update
```
curl localhost:3000/listings/:title \
  -X PUT \
  -H "Content-type:application/json" \
  -d '{"title": "New title of listing", "body": "Different description"}'
```

5. Delete
```
curl localhost:3000/listings/:title -X DELETE
```
