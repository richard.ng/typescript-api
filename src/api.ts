import http from 'http';
import express, { Express } from 'express';
import morgan from 'morgan';
import routes from './routes/listing';

class Api {
  public api: Express = express();

  public Configure() {
    this.api.use(morgan('dev'));
    this.api.use(express.urlencoded({ extended: false }));
    this.api.use(express.json());
  }

  public LoadRoutes() {
    this.api.use('/', routes)
  }

  public Start() {
    const httpServer = http.createServer(this.api);
    const PORT: any = process.env.API_PORT ?? 3000;
    httpServer.listen(PORT, () => {
      console.log(`Express server listening on port ${PORT}`);
    })
  }
}

export default Api;
