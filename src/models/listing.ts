import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const schema = new Schema({
  userId: Number,
  id: Number,
  title: String,
  body: String
});

export default mongoose.model('Listings', schema)
