interface Listing {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export default Listing
