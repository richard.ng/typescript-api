import Api from './api';
import Database from './db';

function main() {
  let db = new Database();
  db.Connect()

  let api = new Api();
  api.Configure()
  api.LoadRoutes()
  api.Start()
}

main()
