import { Request, Response } from 'express';

import Listings from '../models/listing';

export class ListingController {
  public GetListing(req: Request, res: Response) {
    let title = req.params.title

    Listings.findOne({
      title: title
    }, (err: any, data: any) => {
      if (err || !data)
        return res.json({message: "Does not exist"})
      return res.json(data)
    })
  }

  public GetListings(req: Request, res: Response) {
    Listings.find({}, (err: any, data: any) => {
      if (err)
        return res.json({Error: err})
      return res.json(data)
    })
  }

  public NewListing(req: Request, res: Response) {
    const listing = new Listings({
      userId: req.body.userId,
      id: req.body.id,
      title: req.body.title,
      body: req.body.body
    })

    listing.save((err: any, data: any) => {
      if (err)
        return res.json({Error: err})
      else
        console.log("Successfully saved to database:", listing)
        return res.json(data)
    })
  }

  public UpdateListing(req: Request, res: Response) {
    let title = req.params.title

    Listings.updateOne({
      title: title
    },
      req.body,
      (err: any, data: any) => {
        if (data.matchedCount == 0)
          return res.json({message: "Does not exist"})
        else if (data.matchedCount == 1 && data.modifiedCount == 0)
          return res.json({message: "No changes in request body"})
        else if (err)
          return res.json({Error: err})
        else
          return res.json({message: "Successfully updated"})
      })
  }

  public DeleteListing(req: Request, res: Response) {
    let title = req.params.title

    Listings.deleteOne({
      title: title
    }, (err: any, data: any) => {
      if (data.deletedCount == 0)
        return res.json({message: "Does not exist"})
      else if (err)
        return res.json("Something went wrong")
      else
        return res.json({message: "Successfully deleted"})
    })
  }
}
