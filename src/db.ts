import mongoose from 'mongoose';

class Database {
  public mongoUrl: any = process.env.MONGO_URL

  public Connect() {
    mongoose.connect(this.mongoUrl, () => {
      console.log("Database connected")
    })
  }
}

export default Database
