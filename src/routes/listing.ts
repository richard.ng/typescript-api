import express from 'express';
import { ListingController } from '../controllers/listing';

const router = express.Router();

const controller = new ListingController()

router.get('/listings', controller.GetListings);
router.get('/listings/:title', controller.GetListing);
router.post('/listings', controller.NewListing);
router.put('/listings/:title', controller.UpdateListing);
router.delete('/listings/:title', controller.DeleteListing);

export = router;
