db.createUser(
  {
    user: "user",
    pwd: "password",
    roles: [
      { role: "userAdmin", db: "listings" },
      { role: "readWrite", db: "listings" },
      { role: "dbAdmin", db: "listings" }
    ]
  }
);
